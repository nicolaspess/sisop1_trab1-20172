#include "../include/support.h"
#include "../include/cdata.h"
#include "../include/cthread.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>

void init(){
	if(DISPATCHER == NULL){
		ucontext_t *main_ctx, *dispatcher_ctx;
		TCB_t *mainTCB;

		CreateFila2(&aptos);
		CreateFila2(&bloqueados);

		dispatcher_ctx = (ucontext_t *)malloc(sizeof(ucontext_t));
		dispatcher_ctx->uc_stack.ss_sp = malloc(SIGSTKSZ);
		dispatcher_ctx->uc_stack.ss_size = SIGSTKSZ;
		dispatcher_ctx->uc_link = 0;
		getcontext(dispatcher_ctx);
		makecontext(dispatcher_ctx, (void (*)(void))dispatcher,0);
		DISPATCHER = dispatcher_ctx;

		main_ctx = (ucontext_t *)malloc(sizeof(ucontext_t));
		main_ctx->uc_stack.ss_sp = malloc(SIGSTKSZ);
		main_ctx->uc_stack.ss_size = SIGSTKSZ;
		main_ctx->uc_link = 0;

		mainTCB = (TCB_t *)malloc(sizeof(TCB_t));
		mainTCB->tid= 0;
		mainTCB->state=2;//executando
		mainTCB->context=*main_ctx;
		EXECUTANDO = mainTCB;
		getcontext(&mainTCB->context);
	}
}

//sempre que uma thread termina essa função é ativada
//seleciona a proxima thread a ser executada
//e ainda verifica se a thread que terminou estava bloqueando alguma outra, se sim, coloca-a em apto
void dispatcher(){

	JOINBLOCK *bloqStruct;
	TCB_t *prox, *exe, *tcb_aux;
	exe = EXECUTANDO;//thread q estava executando
	exe->state = 4;//termino
	stopTimer();
	
	
	//**********************************************************	
	//AQUI DEVEMOS verificar se existe thread esperando o termino da exe
	//se existe retiramos da fila de bloqueado e colocamos em apto
	//.......

	//**********************************************************
	if(FirstFila2(&bloqueados) == 0){
		while((bloqStruct = GetAtIteratorFila2(&bloqueados))!=NULL){
			if(bloqStruct->id_tcb_dominante == exe->tid){// se a thread que acabou dominava um recurso necessário para bloqstruct->tcb
				tcb_aux = bloqStruct->tcb;
				DeleteAtIteratorFila2(&bloqueados);
				
				if (InsertByPrio(&aptos, tcb_aux) != 0){
					//return -1;
				}
			}
			NextFila2(&bloqueados);
		}
	}

	prox = escalonador(); //primeiro da lista de aptos de acordo com prioridade
	removeDeApto(prox->tid);
	EXECUTANDO = prox;
	//free(exe);
	startTimer();
	swapcontext(&exe->context, &prox->context);
	//setcontext(&prox->context);
}

//seleciona a próxima thread a ser executada de acordo com a prioridade
TCB_t * escalonador(){
	TCB_t *tcb;
	tcb = NULL;
	if(FirstFila2(&aptos) == 0){
		//fila não vazia
		tcb = (TCB_t *)GetAtIteratorFila2(&aptos);
	}
	return tcb;
}

//procura e remove da lista de apto da sua prioridade
void removeDeApto(int id){
	TCB_t *tcb;
	
	FirstFila2(&aptos);
	while((tcb = GetAtIteratorFila2(&aptos))!=NULL){
		if(tcb->tid == id){
			DeleteAtIteratorFila2(&aptos);
			break;
		}
		NextFila2(&aptos);
	}
}

//procura e remove tcb de tid=id
void removeDeBloqueado(int id){
	TCB_t *tcb;
	
	FirstFila2(&bloqueados);
	while((tcb = GetAtIteratorFila2(&bloqueados))!=NULL){
		if(tcb->tid == id){
			DeleteAtIteratorFila2(&bloqueados);
			break;
		}
		NextFila2(&bloqueados);
	}
}

//verifica se o estado da thread passado é apto e ela existe
int verificaTid(int tid){
	//retorna 0 se existe
	//retorna -1 caso não
	TCB_t *tcb;
	FirstFila2(&aptos);

	while((tcb = GetAtIteratorFila2(&aptos))!=NULL){
		if(tcb->tid == tid && tcb->state == 1){
			return 0;
			break;
		}
		NextFila2(&aptos);
	}
	
	return -1;
}