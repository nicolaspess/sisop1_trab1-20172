#include "../include/support.h"
#include "../include/cdata.h"
#include "../include/cthread.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucontext.h>

int ids = 0;

TCB_t *EXECUTANDO = NULL;
ucontext_t *DISPATCHER = NULL;

//cria a thread
int ccreate (void* (*start)(void*), void *arg, int prio){
	TCB_t *newTCB;
	ucontext_t *new_ctx;

	init(); //seta todas as diretrizes de fila

	newTCB = (TCB_t *)malloc(sizeof(TCB_t));
	new_ctx = (ucontext_t *)malloc(sizeof(ucontext_t));

	if(newTCB == NULL){
		return -1;
	}

	new_ctx->uc_stack.ss_sp = malloc(SIGSTKSZ);
	new_ctx->uc_stack.ss_size = SIGSTKSZ;
	new_ctx->uc_link = DISPATCHER;

	newTCB->prio = prio;
	newTCB->state = 1;//apto
	newTCB->tid = ids++;
	newTCB->context = *new_ctx;

	//adiciona na fila de aptos
	//USA INSERT BY PRIO
	//****************************************
	if (InsertByPrio(&aptos, newTCB) != 0){
		return -1;
	}
	
	getcontext(&newTCB->context);
	makecontext(&newTCB->context, (void (*)(void))start, 1, arg);

	return newTCB->tid;
}

//libera o processador
int cyield(void){
	TCB_t *yielder, *prox;

	//*********************************************
	//AQUI DEVEMOS CHAMAR A FUNÇÃO QUE TERMINA A CONTAGEM DO TEMPO DE EXECUÇÃO E ATUALIZA A PRIORIDADE
	//*********************************************
	

	//poe o q tava executando no fim da fila
	yielder = EXECUTANDO;
	yielder->state=1;//apto
	yielder->prio = yielder->prio + stopTimer();


	prox = escalonador();
	if(prox == NULL){
		return -1;
	}
	
	removeDeApto(prox->tid);
	prox->state = 2;
	EXECUTANDO = prox;


	//*********************************************
	// USA INSERT BY PRIO
	//*********************************************
	if (InsertByPrio(&aptos, yielder) != 0){
		return -1;
	}

	//*********************************************
	//AQUI DEVEMOS CHAMAR A FUNÇÃO QUE COMEÇA A CONTAR O TEMPO DE EXECUÇÃO
	//*********************************************
	startTimer();

	swapcontext(&yielder->context, &prox->context);

	return 0;
}

int cjoin(int tid){
	TCB_t *escolhido, *bloqueado;
	JOINBLOCK *bloqStruct;
	
	init();

	//verifica se existe thread com esse id
	if(verificaTid(tid) == -1){
		//thread não existe, ou nao esta em apto
		return -1;
	}
	escolhido = escalonador(); //escolhe o de maior prioridade pra assumir
	if(escolhido == NULL){
		return -1;
	}

	//bloqueado vai esperar pela conclusão da thread de id=tid
	bloqueado = EXECUTANDO;
	bloqueado->state = 3;//bloqueado
	bloqueado->prio = bloqueado->prio + stopTimer();
	//colocamos a thread bloqueada na estrutura referida para a fila de bloqueados
	bloqStruct = (JOINBLOCK *)malloc(sizeof(JOINBLOCK));
	bloqStruct->id_tcb_dominante = tid;
	bloqStruct->tcb = bloqueado;
	
	AppendFila2(&bloqueados, (void*)bloqStruct);
	removeDeApto(escolhido->tid);
	escolhido->state = 2;
	EXECUTANDO = escolhido;
	
	startTimer();
	swapcontext(&bloqueado->context, &escolhido->context);
	
	return 0;
	//a thread será tirada de bloqueado quando a thread aguardade terminar, ou seja, esta parte está implementada em dispatcher.

}

int csem_init(csem_t *sem, int count){
	FILA2 *fila;

	init();

	if(sem == NULL){
		return -1;
	}
	fila = (FILA2*) malloc(sizeof(FILA2));
	if(fila == NULL){
		return -1;
	}
	CreateFila2(fila);
	sem->count = count;
	sem->fila = fila;

	return 0;
}

int cwait(csem_t *sem){
	TCB_t *escolhido, *bloqueado;
	JOINBLOCK *bloqStruct;
	
	init();
	
	if(sem == NULL) return -1;
	
	//atualiza count do semaforo
	sem->count = sem->count - 1;
	
	//bloqueia thread e poe na fila caso semaforo <0
	if(sem->count < 0){
		//pega o proximo a executar
		escolhido = escalonador();
		if(escolhido == NULL){
			return -1;
		}
	
		//coloca na fila do semáforo e bloqueia a thread


		bloqStruct = (JOINBLOCK *)malloc(sizeof(JOINBLOCK));
		if(bloqStruct == NULL) return -1;
		
		bloqueado = EXECUTANDO;
		bloqueado->state = 3;//bloqueado
		bloqueado->prio = bloqueado->prio + stopTimer();
		bloqStruct->tcb = bloqueado;

		
		//add na fila de bloqueados e semaforo
		AppendFila2(&bloqueados, (void*)bloqueado);
		AppendFila2(sem->fila, bloqueado);
		
		removeDeApto(escolhido->tid);
		escolhido->state = 2;//executando
		EXECUTANDO = escolhido;

		startTimer();
		
		swapcontext(&bloqueado->context, &escolhido->context);
	}
	return 0;
}

int csignal(csem_t *sem){
	TCB_t *bloqueado;
	
	init();
	
	if(sem == NULL){
		return -1;
	}
	
	//atualiza semaforo;
	sem->count += 1;
	
	//pega o primeiro da fila do semaforo
	FirstFila2(sem->fila);
	bloqueado = GetAtIteratorFila2(sem->fila);
	
	if(bloqueado){
		//tira a thred das filas e bota em apto
		DeleteAtIteratorFila2(sem->fila);
		bloqueado->state = 1;//apto
		removeDeBloqueado(bloqueado->tid);
		
		AppendFila2(&aptos, (void*)bloqueado);
	}
	return 0;
}

int cidentify (char *name, int size){
	char *nomes = (char *)malloc(sizeof(char *));
	if(name == NULL){
		return -1;
	}
	strcpy(nomes, "Cassiano Bartz 242266, Vinícius Marinho Maschi 231073 e Nícolas Vincent Dall Bello Pessutto 242284");
	name = nomes;
	return 0;
}