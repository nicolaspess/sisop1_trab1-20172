#include <stdio.h>
#include "../include/support.h"
#include "../include/cthread.h"
#define MAX 100
int i=1;
int id0, id1, id2;

void* foo(void *arg){
	int i=1;
	
	for(;i<MAX;i++){
		
		if(i%15==0){
			printf("FOO (i=%d)\n",i);
			cyield();
		}
	}
	return NULL;
}

void* bar(void *arg){
	int i=1;
	
	for(i++;i<130;i++){
		if(i%30==0){
			printf("===========BAR (i=%d)\n",i);
			cyield();
		}
	}
	return NULL;
}

void* lala(void *arg){
	int i=1;
	
	for(i++;i<MAX+1;i++){
		if(i%20==0){
			printf("KKKKK===KKKKK (i=%d)\n",i);
			cyield();
		}
	}
	return NULL;	
}

int main(){

	printf("\n\n");
	
	id0 = ccreate(foo, (void *)&i,0);
	id1 = ccreate(bar, (void *)&i,0);
	id2 = ccreate(lala, (void *)&i,0);	
	
	cjoin(id0); cjoin(id1); cjoin(id2);

	

	printf("\n\n");
	return 0;
}